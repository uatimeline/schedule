const express = require("express");
const passport = require("passport");
const controller = require("../controllers/teacher");
const router = express.Router();

//localhost:5000/api/teacher/
router.post(
  "/",
  passport.authenticate("jwt", { session: false }),
  controller.create
);

//localhost:5000/api/teacher/:universityId
router.get(
  "/:universityId",
  passport.authenticate("jwt", { session: false }),
  controller.getByUniversityId
);

//localhost:5000/api/teacher/:id
router.delete(
  "/:teacherId",
  passport.authenticate("jwt", { session: false }),
  controller.remove
);

router.patch(
  "/:teacherId",
  passport.authenticate("jwt", { session: false }),
  controller.update
);

module.exports = router;
