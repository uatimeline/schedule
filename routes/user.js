const express = require("express");
const controller = require("../controllers/user");
const router = express.Router();

//localhost:5000/api/user/login
router.post("/login", controller.login);

//localhost:5000/api/user/register
router.post("/register", controller.register);

module.exports = router;
