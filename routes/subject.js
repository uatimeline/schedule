const express = require("express");
const passport = require("passport");
const controller = require("../controllers/subject");
const router = express.Router();

//localhost:5000/api/subjec/
router.post("/", 
passport.authenticate("jwt", { session: false }),
controller.create);

//localhost:5000/api/subjec/:facultyId
router.get(
  "/:universityId",
  passport.authenticate("jwt", { session: false }),
  controller.getByUniversityId
);

//localhost:5000/api/subjec/:id
router.delete(
  "/:subjectId",
  passport.authenticate("jwt", { session: false }),
  controller.remove
);

router.patch(
  "/:subjectId",
  passport.authenticate("jwt", { session: false }),
  controller.update
);

module.exports = router;
