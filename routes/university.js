const express = require("express");
const passport = require("passport");
const controller = require("../controllers/university");
const router = express.Router();

//localhost:5000/api/univer/register
router.post(
  "/register",
  passport.authenticate("jwt", { session: false }),
  controller.create
);

//localhost:5000/api/univer/:id
router.post(
  "/",
  passport.authenticate("jwt", { session: false }),
  controller.get
);

//localhost:5000/api/univer/
router.get(
  "/",
  passport.authenticate("jwt", { session: false }),
  controller.getAll
);

//localhost:5000/api/univer/:id
router.delete(
  "/:id",
  passport.authenticate("jwt", { session: false }),
  controller.remove
);
//localhost:5000/api/univer/:id
router.patch(
  "/:id",
  passport.authenticate("jwt", { session: false }),
  controller.update
);

module.exports = router;
