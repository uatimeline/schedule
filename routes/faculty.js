const express = require("express");
const passport = require("passport");
const controller = require("../controllers/faculty");
const router = express.Router();

//localhost:5000/api/faculty/
router.post(
  "/",
  passport.authenticate("jwt", { session: false }),
  controller.create
);

//localhost:5000/api/faculty/:universityId
router.get(
  "/:universityId",
  passport.authenticate("jwt", { session: false }),
  controller.getByUniversityId
);

//localhost:5000/api/faculty/:universityId
router.delete(
  "/:facultyId",
  passport.authenticate("jwt", { session: false }),
  controller.remove
);

router.patch(
  "/:facultyId",
  passport.authenticate("jwt", { session: false }),
  controller.update
);

module.exports = router;
