const express = require("express");
const controller = require("../controllers/group");
const passport = require("passport");
const router = express.Router();

//localhost:5000/api/group/
router.post("/", passport.authenticate("jwt", { session: false }), controller.create);

//localhost:5000/api/group/:facultyId
router.get(
  "/:facultyId",
  passport.authenticate("jwt", { session: false }),
  controller.getByFacultyId
);

//localhost:5000/api/group/:id
router.delete(
  "/:groupId",
  passport.authenticate("jwt", { session: false }),
  controller.remove
);

router.patch(
  "/:groupId",
  passport.authenticate("jwt", { session: false }),
  controller.update
);

module.exports = router;
