const express = require("express");
const passport = require("passport");
const controller = require("../controllers/timetable");
const router = express.Router();

//localhost:5000/api/timetable/
router.post(
  "/",
  passport.authenticate("jwt", { session: false }),
  controller.create
);

//localhost:5000/api/timetable/:groupId
router.get(
  "/:groupId",
  passport.authenticate("jwt", { session: false }),
  controller.getByGroupId
);

//localhost:5000/api/timetable/:id
router.delete(
  "/:groupId",
  passport.authenticate("jwt", { session: false }),
  controller.remove
);

router.patch(
  "/:groupId",
  passport.authenticate("jwt", { session: false }),
  controller.update
);

module.exports = router;
