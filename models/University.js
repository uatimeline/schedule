const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const universitySchema = new Schema({
  name: {
    type: String,
    required: true,
    // because create user with name univer
    unique: true
  },
  address: {
    type: String,
    default: ""
  },
  timelesson: [
    {
      start: {
        type: String
      },
      end: {
        type: String
      }
    }
  ]
});
module.exports = mongoose.model("universities", universitySchema);
