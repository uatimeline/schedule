const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const subjectSchema = new Schema({
  name: {
    type: String,
    required: true
  },

  university: {
    ref: "universities",
    type: Schema.Types.ObjectId
  }
});
module.exports = mongoose.model("subjects", subjectSchema);
