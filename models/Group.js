const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const groupSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  yearAdminssion: {
    type: Number,
    required: true
  },
  faculty: {
    ref: "faculties",
    type: Schema.Types.ObjectId
  }
});
module.exports = mongoose.model("groups", groupSchema);
