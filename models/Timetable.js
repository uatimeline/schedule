const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const timetableSchema = new Schema({
  group: {
    ref: "groups",
    type: Schema.Types.ObjectId
  },
  teacher: {
    ref: "teachers",
    type: Schema.Types.ObjectId
  },
  subject: {
    ref: "subjects",
    type: Schema.Types.ObjectId
  },
  numberLesson: {
    type: Number,
    required: true
  },
  lectureHall: {
    type: String,
    required: true
  },
  date: {
    type: Date,
    default: Date.now
  }
});
module.exports = mongoose.model("timetables", timetableSchema);
