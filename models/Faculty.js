const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const facultySchema = new Schema({
  name: {
    type: String,
    required: true
  },
  tel: {
    type: Number,
    default: ""
  },
  university: {
    ref: "universities",
    type: Schema.Types.ObjectId
  }
});
module.exports = mongoose.model("faculties", facultySchema);
