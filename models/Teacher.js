const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const teacherSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  surname: {
    type: String,
    required: true
  },
  login: {
    type: String,
    required: true
  },
  password: {
    type: String,
    required: true
  },
  isAdmin: {
    type: Boolean,
    default: false
  },
  university: {
    ref: "universities",
    type: Schema.Types.ObjectId
  }
});
module.exports = mongoose.model("teachers", teacherSchema);
