const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const usersSchema = new Schema({
  name: {
    type: String,
    required: true,
    unique: true
  },
  email: {
    type: String,
    required: true,
    unique: true
  },
  password: {
    type: String,
    required: true
  },
  group: {
    ref: "groups",
    type: Schema.Types.ObjectId
  }
});
module.exports = mongoose.model("users", usersSchema);
