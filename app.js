const express = require("express");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");
const passport = require("passport");
const userRoutes = require("./routes/user");
const adminRoutes = require("./routes/admin");
const facultyRoutes = require("./routes/faculty");
const universityRoutes = require("./routes/university");
const groupRoutes = require("./routes/group");
const subjectRoutes = require("./routes/subject");
const timetableRoutes = require("./routes/timetable");
const teacherRoutes = require("./routes/teacher");
const cors = require("cors");
const morgan = require("morgan");
const keys = require("./config/keys");
const app = express();

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
mongoose
  .connect(keys.mongoURI)
  .then(() => {
    console.log("Mongo connected!");
  })
  .catch(error => console.log(error));

app.use(passport.initialize());

require("./middleware/passport")(passport);

app.use(morgan("dev"));
app.use(cors());
app.use("/api/admin", adminRoutes);
app.use("/api/user", userRoutes);
app.use("/api/univer", universityRoutes);
app.use("/api/faculty", facultyRoutes);
app.use("/api/teacher", teacherRoutes);
app.use("/api/timetable", timetableRoutes);
app.use("/api/group", groupRoutes);
app.use("/api/subject", subjectRoutes);

module.exports = app;
