const Faculty = require("../models/Faculty");
const Group = require("../models/Group");
const User = require("../models/User");
const TimeTable = require("../models/Timetable");
const errorHandler = require("../utils/errorHandler");
const University = require("../models/University");

module.exports.getByUniversityId = async function(req, res) {
  try {
    const faculties = await Faculty.find({
      university: req.params.universityId
    });
    res.status(200).json(faculties);
  } catch (error) {
    errorHandler(res, error);
  }
};

module.exports.remove = async function(req, res) {
  try {
    const groupsForRemove = await Group.find(
      { faculty: req.params.facultyId },
      async function(err, groupsForRemove) {
        groupsForRemove.forEach(async elem => {
          await TimeTable.remove({ group: elem._id });
          await User.remove({ group: elem._id });
          await Group.remove({ _id: elem._id });
        });
      }
    );
    await Faculty.remove({ _id: req.params.facultyId });
    res.status(200).json({
      message: "Faculty is deleted"
    });
  } catch (error) {
    errorHandler(res, error);
  }
};

module.exports.create = async function(req, res) {
  try {
    University.findOne({ name: req.body.universityName }).exec(async function(
      err,
      nameUniver
    ) {
      if (err || nameUniver === null) throw new Error("University not found");
      const faculty = await new Faculty({
        name: req.body.name,
        tel: req.body.tel,
        university: nameUniver._id
      }).save();

      res.status(201).json(faculty);
    });
  } catch (error) {
    errorHandler(res, error);
  }
};

module.exports.update = async function(req, res) {
  try {
    const faculty = await Faculty.findOneAndUpdate(
      {
        _id: req.params.facultyId
      },
      { $set: req.body },
      { new: true }
    );
    res.status(200).json(faculty);
  } catch (error) {
    errorHandler(res, error);
  }
};
