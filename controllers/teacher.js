const bcrypt = require("bcryptjs");
const Teacher = require("../models/Teacher");
const University = require("../models/University");
const TimeTable = require("../models/Timetable");
const errorHandler = require("../utils/errorHandler");

module.exports.getByUniversityId = async function(req, res) {
  try {
    const teachersUniver = await Teacher.find({
      university: req.params.universityId
    });
    res.status(200).json(teachersUniver);
  } catch (error) {
    errorHandler(res, error);
  }
};

module.exports.remove = async function(req, res) {
  try {
    await TimeTable.remove({ teacher: req.params.teacherId });
    await Teacher.remove({ _id: req.params.teacherId });
    res.status(200).json({
      message: "Teacher is deleted"
    });
  } catch (error) {
    errorHandler(res, error);
  }
};

module.exports.create = async function(req, res) {
  try {
    University.findOne({ name: req.body.universityName }).exec(async function(
      err,
      nameUniver
    ) {
      if (err || nameUniver === null) throw new Error("University not found");
      const salt = bcrypt.genSaltSync(10);
      const password = req.body.password;
      const teacher = await new Teacher({
        name: req.body.name,
        surname: req.body.surname,
        login: req.body.login,
        password: bcrypt.hashSync(password, salt),
        isAdmin: req.body.isAdmin,
        university: nameUniver._id
      }).save();

      res.status(201).json(teacher);
    });
  } catch (error) {
    errorHandler(res, error);
  }
};

module.exports.update = async function(req, res) {
  try {
    const teacher = await Teacher.findOneAndUpdate(
      {
        _id: req.params.teacherId
      },
      { $set: req.body },
      { new: true }
    );
    res.status(200).json(teacher);
  } catch (error) {
    errorHandler(res, error);
  }
};
