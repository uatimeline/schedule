const University = require("../models/University");
const Subject = require("../models/Subject");
const Faculty = require("../models/Faculty");
const User = require("../models/User");
const Teacher = require("../models/Teacher");
const TimeTable = require("../models/Timetable");
const errorHandler = require("../utils/errorHandler");

module.exports.create = async function(req, res) {
  try {
    const univer = new University({
      name: req.body.name,
      adress: req.body.adress,
      timelesson: req.body.timelesson
    });
    await univer.save().then(() => console.log("Univer created"));
  } catch (e) {
    errorHandler(res, e);
  }
};

module.exports.getAll = async function(req, res) {
  try {
    const universities = await University.find({});
    res.status(200).json(universities);
  } catch (error) {
    errorHandler(res, error);
  }
};

module.exports.remove = async function(req, res) {
  try {
    await Faculty.find({ university: req.params.id }, async function(
      err,
      facultiesForRemove
    ) {
      facultiesForRemove.forEach(async elem => {
        await Group.find({ faculty: elem._id }, async function(
          err,
          groupForRemove
        ) {
          groupForRemove.forEach(async item => {
            await TimeTable.remove({ group: item._id });
            await User.remove({ group: item._id });
            await Group.remove({ _id: item._id });
          });
        });
        await Faculty.remove({ university: req.params.id });
      });
    });
    await University.remove({ _id: req.params.id });
    res.status(200).json({
      message: "University is deleted"
    });
  } catch (error) {
    errorHandler(res, error);
  }
};

module.exports.update = async function(req, res) {
  try {
    const university = await University.findOneAndUpdate(
      {
        _id: req.params.id
      },
      { $set: req.body },
      { new: true }
    );
    res.status(200).json(university);
  } catch (error) {
    errorHandler(res, error);
  }
};

module.exports.get = async function(req, res) {
  try {
  } catch (error) {
    errorHandler(res, error);
  }
};
