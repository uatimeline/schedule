const TimeTable = require("../models/Timetable");
const Group = require("../models/Group");
const Subject = require("../models/Subject");
const Teacher = require("../models/Teacher");
const errorHandler = require("../utils/errorHandler");

module.exports.getByGroupId = async function(req, res) {
  try {
    const timetableGroup = await TimeTable.find({
      group: req.params.groupId
    });
    res.status(200).json(timetableGroup);
  } catch (error) {
    errorHandler(res, error);
  }
};

module.exports.remove = async function(req, res) {
  try {
    await TimeTable.remove({ _id: req.params.groupId });
    res.status(200).json({
      message: "TimeTable is deleted"
    });
  } catch (error) {
    errorHandler(res, error);
  }
};

module.exports.create = async function(req, res) {
  try {
    await Group.findOne({ name: req.body.groupName }, "_id", async function(
      err,
      idGroupName
    ) {
      await Teacher.findOne({ name: req.body.teacherName }),
        "_id",
        async function(err, idTeacerName) {
          await Subject.findOne(
            { name: req.body.subjectName },
            "_id",
            async function(err, idSubjectName) {
              const timetable = new TimeTable({
                group: idGroupName._id,
                teacher: idTeacerName._id,
                subject: idSubjectName._id,
                numberLesson: req.body.numberLesson,
                lectureHall: req.body.lectureHall,
                date: req.body.date
              });
            }
          );
        };
    });
  } catch (error) {
    errorHandler(res, error);
  }
};

module.exports.update = async function(req, res) {
  try {
    const timetable = await TimeTable.findOneAndUpdate(
      {
        _id: req.params.id
      },
      { $set: req.body },
      { new: true }
    );
    res.status(200).json(timetable);
  } catch (error) {
    errorHandler(res, error);
  }
};
