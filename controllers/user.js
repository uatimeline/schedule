const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const keys = require("../config/keys");
const User = require("../models/User");
const Group = require("../models/Group");
const errorHandler = require("../utils/errorHandler");

module.exports.login = async function(req, res) {
  const candidate =
    (await User.findOne({ email: req.body.name })) ||
    (await User.findOne({ name: req.body.name }));
  if (candidate) {
    const passwordResult = bcrypt.compareSync(
      req.body.password,
      candidate.password
    );
    if (passwordResult) {
      // Generate token check!!!
      const token = jwt.sign(
        {
          name: candidate.name,
          userId: candidate._id
        },
        keys.jwt,
        { expiresIn: keys.timeToken }
      );
      res.status(200).json({
        token: `Bearer ${token}`
      });
    } else {
      res.status(401).json({
        message: "Wrong password. Try again"
      });
    }
  } else {
    res.status(404).json({
      message: "User with such name or email not found. Try again"
    });
  }
};

module.exports.register = async function(req, res) {
  // check it for working!!!!
  const candidate =
    (await User.findOne({ email: req.body.email })) ||
    (await User.findOne({ name: req.body.name }));

  if (candidate) {
    res.status(409).json({
      message: "Login or email already exists."
    });
  } else {
    try {
      Group.findOne({ name: req.body.group }).exec(async function(
        err,
        nameGroup
      ) {
        console.log(nameGroup);
        if (err || nameGroup === null) throw new Error("nameGroup not found");
        const salt = bcrypt.genSaltSync(10);
        const password = req.body.password;
        const user = new User({
          name: req.body.name,
          email: req.body.email,
          password: bcrypt.hashSync(password, salt),
          group: nameGroup._id
        });
        await user.save().then(() => console.log("User created"));
        res.status(201).json(user);
      });
    } catch (e) {
      errorHandler(res, e);
    }
  }
};
