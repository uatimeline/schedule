const Group = require("../models/Group");
const Faculty = require("../models/Faculty");
const TimeTable = require("../models/Timetable");
const User = require("../models/User");
const errorHandler = require("../utils/errorHandler");

module.exports.getByFacultyId = async function(req, res) {
  try {
    const groupsFaculty = await Group.find({
      faculty: req.params.facultyId
    });
    res.status(200).json(groupsFaculty);
  } catch (error) {
    errorHandler(res, error);
  }
};

module.exports.remove = async function(req, res) {
  try {
    await TimeTable.remove({ group: req.params.groupId });
    await User.remove({ group: req.params.groupId });
    await Group.remove({ _id: req.params.groupId });
    res.status(200).json({
      message: "Group is deleted"
    });
  } catch (error) {
    errorHandler(res, error);
  }
};

module.exports.create = async function(req, res) {
  try {
    Faculty.findOne({ name: req.body.facultyName }).exec(async function(
      err,
      nameFaculty
    ) {
      if (err || nameFaculty === null) throw new Error("Faculty not found");
      const group = await new Group({
        name: req.body.name,
        yearAdminssion: req.body.yearAdminssion,
        faculty: nameFaculty._id
      }).save();

      res.status(201).json(group);
    });
  } catch (error) {
    errorHandler(res, error);
  }
};

module.exports.update = async function(req, res) {
  try {
    const group = await Group.findOneAndUpdate(
      {
        _id: req.params.groupId
      },
      { $set: req.body },
      { new: true }
    );
    res.status(200).json(group);
  } catch (error) {
    errorHandler(res, error);
  }
};
