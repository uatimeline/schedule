const Subject = require("../models/Subject");
const TimeTable = require("../models/Timetable");
const University = require("../models/University");
const errorHandler = require("../utils/errorHandler");

module.exports.getByUniversityId = async function(req, res) {
  try {
    const subjectsUniver = await Subject.find({
      university: req.params.universityId
    });
    res.status(200).json(subjectsUniver);
  } catch (error) {
    errorHandler(res, error);
  }
};

module.exports.remove = async function(req, res) {
  try {
    await TimeTable.remove({ subject: req.params.subjectId });
    await Subject.remove({ _id: req.params.subjectId });
    res.status(200).json({
      message: "Subject is deleted"
    });
  } catch (error) {
    errorHandler(res, error);
  }
};

module.exports.create = async function(req, res) {
  try {
    University.findOne({ name: req.body.universityName }).exec(async function(
      err,
      nameUniver
    ) {
      if (err || nameUniver === null) throw new Error("University not found");
      const subject = await new Subject({
        name: req.body.name,
        university: nameUniver._id
      }).save();
      res.status(201).json(subject);
    });
  } catch (error) {
    errorHandler(res, error);
  }
};

module.exports.update = async function(req, res) {
  try {
    const subject = await Subject.findOneAndUpdate(
      {
        _id: req.params.subjectId
      },
      { $set: req.body },
      { new: true }
    );
    res.status(200).json(subject);
  } catch (error) {
    errorHandler(res, error);
  }
};
